# Welcome to pyfrigg 🕸️
[![Codestyle: Black](https://img.shields.io/badge/codestyle-black-black.svg)](https://github.com/psf/black)
[![Codecheck: Flake8](https://img.shields.io/badge/codecheck-flake8-blue.svg)](https://gitlab.com/pycqa/flake8)

## Description

High level wrappers of various python db and web libraries for loading data with maximum result and less effort.
Supports asynchronous requests with interaction Elasticsearch, MySQL, and more!

## Modules

* `elasticsearch`: wrapper of `elasticsearch`
* `mysql`: wrapper of `mysql-connector-python`
* `requests`: wrapper of `requests` and `aiohttp`

## Authors

* Nick Kuzmenkov
* Dmitry Bulychev